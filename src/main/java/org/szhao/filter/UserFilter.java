package org.szhao.filter;

/**
 * @author zsh
 * created 12/Jan/2021
 */
public class UserFilter {
    public final String username;
    public final int age;

    public UserFilter(String username, int age) {
        this.username = username;
        this.age = age;
    }

    @Override
    public String toString() {
        return "UserFilter{" +
            "username='" + username + '\'' +
            ", age=" + age +
            '}';
    }
}
