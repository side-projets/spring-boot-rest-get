package org.szhao.filter;

/**
 * @author zsh
 * created 12/Jan/2021
 */
public class User {
    public final String username;
    public final int age;
    public final String address;

    public User(String username, int age, String address) {
        this.username = username;
        this.age = age;
        this.address = address;
    }
}
