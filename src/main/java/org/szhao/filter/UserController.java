package org.szhao.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zsh
 * created 12/Jan/2021
 */
@RestController
@RequestMapping("/users")
public class UserController {
    public static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @GetMapping
    public ResponseEntity<List<User>> getUsers(UserFilter filter) {
        LOG.info("received filter: {}", filter);

        return ResponseEntity.ok(new ArrayList<>());
    }
}
